# Steps to install runner on the cluster using helm 

1. 

helm repo add gitlab htts://charts.gitlab.io
helm repo update 
helm search repo -l gitlab/gitlab-runner 
helm install --namespace gitlab gitlab-runner -f values.yml gitlab/gitlab-runner
