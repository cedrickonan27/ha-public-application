 # Adding Backend as S3 for Remote State Storage

    terraform {
    backend "s3" {
    bucket = "ha-env-back-end"
    key    = "dev/terraform.tfstate" # you can modify it as you want 
    region = "us-east-2" 

    # Enable during Step-09     
    # For State Locking
    dynamodb_table = "ha-end-state-table"    
  }
    }